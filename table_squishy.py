from __future__ import division
import relax as rx
import self_manip
import numpy as np
import math
import matplotlib.pyplot as plt
import copy as cp
import itertools as it
import functools as ft
import argparse
import sys
from scipy.linalg import block_diag
from self_manip import dot, inv
import matplotlib.animation as animation

# a simple example of a squishy table
g = 1.
l0 = 1/2. #leg length
w = l0/2.
kappa = 27 
#w = .5 #half width

# q = [ xr, yr, xf, yf, x,y,theta]
# r -rear
# f - front

M = np.eye(7)
Minv = np.linalg.inv(M)

# univariate constraints
def a(t,q,index):
    return q[index]

def Da(t,q,index):
    mat = np.zeros(7)
    mat[index] = 1.
    return mat

#bilateral constraints

def DaFL(t,q):
    xr,zr,xf,zf,x,z,t = q[:7]
    Da = np.array([0, 0, 2*xf - 2*z - 1.0*np.cos(t), -2*x + 2*zf - 1.0*np.sin(t), 
        2*x - 2*zf + 1.0*np.sin(t), -2*xf + 2*z + 1.0*np.cos(t), 
        1.0*(x - zf + 0.5*np.sin(t))*np.cos(t) - 1.0*(-xf + z + 0.5*np.cos(t))*np.sin(t)])
    return Da

def DaRL(t,q):
    xr,zr,xf,zf,x,z,t = q[:7]
    Da = np.array( [2*xr - 2*z + 1.0*np.cos(t), -2*x + 2*zr + 1.0*np.sin(t), 0, 0, 
        2*x - 2*zr - 1.0*np.sin(t), -2*xr + 2*z - 1.0*np.cos(t), 
        -1.0*(x - zr - 0.5*np.sin(t))*np.cos(t) + 1.0*(-xr + z - 0.5*np.cos(t))*np.sin(t)])
            
    return Da

def DaFA(t,q):
    #derivative of the angle constraint
    xr,zr,xf,zf,x,z,t = q[:7]

    Da = np.array([0, 0, -np.cos(t), -np.sin(t), np.sin(t), np.cos(t), x*np.cos(t) + xf*np.sin(t) - z*np.sin(t) - zf*np.cos(t)])
    return Da
    
def DaRA(t,q):
    #derivative of the angle constraint
    xr,zr,xf,zf,x,z,t = q[:7]
    Da = np.array( [np.cos(t), np.sin(t), 0, 0, -np.sin(t), -np.cos(t),-x*np.cos(t) - xr*np.sin(t) + z*np.sin(t) + zr*np.cos(t)])
    return Da    

# helper functions for model
def lF(q,symbolic=False):
    xr,zr,xf,zf,x,z,t = q[:7]
    if symbolic:
        raise NotImplementedError
    else:
        return math.sqrt( math.pow(x+w*np.cos(t)-xf,2)+math.pow(z+w*np.sin(t)-zf,2) )

def lR(q,symbolic=False):
    xr,zr,xf,zf,x,z,t = q[:7]
    if symbolic:
        raise NotImplementedError
    else:
        return math.sqrt( math.pow(x-w*np.cos(t)-xr,2)+math.pow(z-w*np.sin(t)-zr,2) )

def Dlf(q, symbolic=False):
    xr,zr,xf,zf,x,z,t = q[:7]
    L = lF(q,symbolic)
    # orient Dl(x) correctly
    return -1*np.array([ 0., 0., -(x+w*np.cos(t)-xf)/L, -(z+w*np.sin(t)-zf)/L, 
        (x+w*np.cos(t)-xf)/L, (z+w*np.sin(t)-zf)/L, 
        (-w*np.sin(t)*(x+w*np.cos(t)-xf)+w*np.cos(t)*(z+w*np.sin(t)-zf))/L ])

def Dlr(q, symbolic=False):
    xr,zr,xf,zf,x,z,t = q[:7]
    L = lR(q,symbolic)
    # orient Dl(x) correctly
    return -1*np.array([ -(x-w*np.cos(t)-xr)/L, -(z-w*np.sin(t)-zr)/L, 0., 0.,
        (x-w*np.cos(t)-xr)/L, (z-w*np.sin(t)-zr)/L, 
        (w*np.sin(t)*(x-w*np.cos(t)-xr)-w*np.cos(t)*(z-w*np.sin(t)-zr))/L ])

def vf(kappa,q,symbolic=False):
    return kappa*(lF(q,symbolic) - l0)

def vr(kappa,q,symbolic=False):
    return kappa*(lR(q,symbolic) - l0)

#vector fields
def f(t,x,p):
    G = np.array([0,-g,0,-g,0,-g,0])
    return np.dot(M,G)

def f_springLegf(t,q,p,contact=False, symbolic=False):
    #spring in leg
    fI = np.dot( vf(kappa*(contact+1),q,symbolic), Dlf(q,symbolic) )
    return fI

def f_springLegr(t,q,p,contact=False, symbolic=False):
    #spring in leg
    fI = np.dot( vr(kappa*(contact+1),q,symbolic), Dlr(q,symbolic) )
    return fI

def f_torque(t,q,p):
    #add a body torque dependent upon difference in foot vertical velocites

    footdiff_zvel = np.abs(q[1+7]-q[3+7])
    footdiff_zvel = (q[1+7]-q[3+7])**2
    f = np.array([0,0,0,0,0,0,.75])*footdiff_zvel
    return f

def ic(theta, h = 1.0, lval =l0, w0 = w):
    '''
    theta - table tilt(radians)
    h - initial height of table top (of untilted table)
    l - length of table leg
    '''
    x = np.zeros(14)
    #table frame
    x[0] = -w0 
    x[1] = -lval
    x[2] = +w0
    x[3] = -lval

    R = np.array([ [ np.cos(theta), -1*np.sin(theta)],
                   [ np.sin(theta), np.cos(theta)]])
    for i in range(2):
        x[2*i:2*i+2] = np.dot(R, x[2*i:2*i+2])

    #global frame translation
    x[1] = h+x[1]
    x[3] = h+x[3]
    x[4] = 0
    x[5] = h
    x[6] = theta 

    return x

def add_functions(*functions):
    # add functions that take three arguments
    def add(f,g):
        return lambda t,x,p: f(t,x,p) + g(t,x,p)
    return ft.reduce(add, functions)

def generate_table_model(rigid=False, pcr=False):
    table = self_manip.SelfManip(M)
    table._a_list = [ft.partial(a, index=1), ft.partial(a,index=3)]
    table._Da_list = [ft.partial(Da, index=1), ft.partial(Da,index=3)]
    table._Dadt_list = [lambda t,q: np.zeros(7), lambda t,q: np.zeros(7)]
    # rigid table
    if rigid:
        table._Da_bilateral = [ ft.partial(DaFL),
                                  ft.partial(DaRL),
                                  DaFA, DaRA]
    else:
        #springy table
        table._Da_bilateral = []#, DaA1, DaA2 ]
        table._Dadt_bilateral = []
                             
    # the vector field is the same
    f_dict = {}
    for key in it.product([True,False],repeat=2):
        if rigid:
            f_dict[key] = f
        if not rigid:
            f_dict[key] = add_functions(f,f_springLegf,
                                           f_springLegr)
                    
            if pcr:
                f_dict[key] = add_functions(f_torque, f_dict[key])


    table._f = f_dict
    table.num_q = 7
    return table

def run_sweep(pcr=True):
    table=generate_table_model(False, pcr)

    p = table.P()
    p.j = np.array([False, False])
    rx_param = 1e-8
    n = np.infty
    debug = False

    thetas = np.arange(-0.3, 0.31 , .01)
    t = (0,1.3)
    h= 1e-3

    thetas[np.abs(thetas)<1e-6] = 0. #force 0 rotation to be a value
    theta_dots = np.zeros_like(thetas)
    y_dots = np.zeros_like(thetas)
    x_dots = np.zeros_like(thetas)
    for index, theta in enumerate(thetas):
        sys.stdout.write('.')
        sys.stdout.flush()
        x0 = ic(theta, h=1.0)
        trjs = rx.EulerSelfManipulation(t, x0, p, table, h, rx_param, 4, debug);
        trj = trjs[-1]

        qdot = trj.x[0][-7:]
        qdot = trj.x[-1][-7:]
        theta_dots[index] = qdot[-1]
    return thetas, theta_dots

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--animate", action="store_true")
    # sweep over the theta dot values
    #parser.add_argument("--sweep", action="store_true")
    parser.add_argument("theta", type=float, nargs='?',   default=0.)
    args = parser.parse_args()

    table=generate_table_model(False, True)

    t = (0,1.3)
    p = table.P()
    p.j = np.array([False, False])
    h =  1e-3
    rx_param = 1e-8
    n = np.infty
    debug = False

    if True: #sweep over thetas
        thetas = np.arange(-0.3, 0.31 , .01)
        t = (0,1.3)
        h= 1e-3

        trjsList = []
        thetas[np.abs(thetas)<1e-6] = 0. #force 0 rotation to be a value
        theta_dots = np.zeros_like(thetas)
        y_dots = np.zeros_like(thetas)
        x_dots = np.zeros_like(thetas)
        for index, theta in enumerate(thetas):
            x0 = ic(theta, h=1.0)
            trjs = rx.EulerSelfManipulation(t, x0, p, table, h, rx_param, 4, debug);
            trjsList.append(cp.deepcopy(trjs))
            trj = trjs[-1]

            qdot = trj.x[0][-7:]
            qdot = trj.x[-1][-7:]
            theta_dots[index] = qdot[-1]
            y_dots[index] = qdot[-2]
            x_dots[index] = qdot[-3]
        plt.figure()
        plt.plot(thetas, theta_dots, '-*') 
        if rigid:
            plt.title('Rigid')
        else:
            plt.title('Squishy')

        #plt.legend(['Theta Dot','Z Dot', 'X dot'])
        plt.legend(['Theta Dot'])
        plt.xlabel('Theta')
        plt.ylabel('Dot')
    
