import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager

import table_squishy
import table_rigid

print('Running piecewise--differentiable model')
squishy = table_squishy.run_sweep(pcr=True)
print('')
print('Running discontinuous model')
rigid = table_rigid.run_sweep()

data = np.vstack( (squishy[0], rigid[1], squishy[1] ) ).T

x = data
font = {'family':'sans-serif','size':20}
mpl.rc('font',**font)
n = (x[:,0]==0.).nonzero()[0][0]
xlim = (-.3,.3)
lwl = 8
lwn = 4
ms  = 12

thl,dth1l,_ = x[:n].T
thr,dth1r,_ = x[n+1:].T

####
# stiff leg plot
####

k = 6

s1l = (np.diff(dth1l[-k:]) / np.diff(thl[-k:])).mean()
s1r = (np.diff(dth1r[:+k]) / np.diff(thr[:+k])).mean()

#compute limit of dtheta at theta=0
dtheta0l = dth1l[0]-s1l*(thl[0]-0.)
dtheta0r = dth1r[0]-s1r*(thr[0]-0.)


thl = np.hstack([thl, 0.])
dth1l = np.hstack([dth1l, dtheta0l])
thr = np.hstack([0., thr])
dth1r = np.hstack([dtheta0r, dth1r])



fig = plt.figure(figsize=(12,4)); plt.clf()

ylim = (-.5,.5)
ax = plt.subplot(1,2,1); ax.grid('on')

ax.plot(xlim,[0.,0.],'k-',lw=lwn)

ax.plot(thl,dth1l,'r-',lw=lwn)
ax.plot(thl[-1],dth1l[-1],'o',mec='r',mfc='w',ms=ms,mew=lwn)

ax.plot(thr,dth1r,'b-',lw=lwn)
ax.plot(thr[0],dth1r[0],'o',mec='b',mfc='w',ms=ms,mew=lwn)

ax.plot(0.,0.,'o',mec='purple', mfc='purple',ms=ms)

ax.set_xlabel(r'initial rotation $\theta(0)$ (rad)')
ax.set_ylabel(r'final rotational velocity $\dot{\theta}(t)$'
                '\n'
                r'(rad/sec)')

ax.set_xlim(xlim)
ax.set_ylim(ylim)

yfill = [ylim[0], ylim[1], ylim[1], ylim[0] ]
w1xfill = [0, 0, xlim[1], xlim[1]]

ax.set_xticklabels(ax.get_xticks(), font)
ax.set_yticklabels(ax.get_yticks(), font)

####
# soft leg plot
####

thl,_,dth2l = x[:n+1].T
thr,_,dth2r = x[n:].T


k = 4
s2l = (np.diff(dth2l[-k:]) / np.diff(thl[-k:])).mean()
s2r = (np.diff(dth2r[:+k]) / np.diff(thr[:+k])).mean()


ylim = (-.45,.45)
yfill = [ylim[0], ylim[1], ylim[1], ylim[0] ]
wlxfill = [0, 0, xlim[1], xlim[1]]
wrxfill = [0, 0, xlim[0], xlim[0]]
ax = plt.subplot(1,2,2); ax.grid('on')

ax.plot(xlim,[0.,0.],'k-',lw=lwn)

ax.plot(thl,dth2l[-1]+s2l*thl,'--',lw=lwl,color=(1.,.5,.5))
ax.plot(thl,dth2l,'r-',lw=lwn)
ax.plot(thl[-1],dth2l[-1],'o',mec='r',mfc='r',ms=ms)

ax.plot(thr,dth2r[0]+s2r*thr,'--',lw=lwl,color=(.5,.5,1.))
ax.plot(thr,dth2r,'b-',lw=lwn)
ax.plot(thr[0],dth2r[0],'o',mec='b',mfc='b',ms=ms)

ax.plot(thr[0],dth2r[0],'o',mec='purple',mfc='purple',ms=ms)

ax.set_xlabel(r'initial rotation $\theta(0)$ (rad)')

ax.set_xlim(xlim)
ax.set_ylim(ylim)

ax.set_xticklabels(ax.get_xticks(), font)
ax.set_yticklabels(ax.get_yticks(), font)


fig.savefig('Figure1_PaceBurden2017.pdf',bbox_inches='tight',pad_inches=0.)
print('')
print('Figure saved in Figure1_PaceBurden2017.pdf')
