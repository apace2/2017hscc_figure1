from __future__ import division
import relax as rx
import numpy as np
import math
import matplotlib.pyplot as plt
import copy as cp
import self_manip
import itertools as it
import functools as ft
import argparse
import sympy as sym
import sys
from scipy.linalg import block_diag
from self_manip import dot, inv
import matplotlib.animation as animation

# a simple example of a rigid table
g = 1.
l = 1/2. #leg length
w = l/2
w = l
#w = .5 #half width

# q = [ xr, yr, xf, yf, x,y,theta]
# r -rear
# f - front

M = np.eye(3)
Minv = np.linalg.inv(M)

# unilateral constraints

def af(t,q):
    return w*np.sin(q[2])-l*np.cos(q[2])+q[1]
def ar(t,q):
    return -w*np.sin(q[2])-l*np.cos(q[2])+q[1]

def Daf(t,q):
    return np.array([0., 1., l*np.sin(q[2])+w*np.cos(q[2])])
def Dar(t,q):
    return np.array([0., 1., l*np.sin(q[2])-w*np.cos(q[2])])



# helper functions for model
def lF(q,symbolic=False):
    xr,zr,xf,zf,x,z,t = q[:7]
    if symbolic:
        raise NotImplementedError
        #return sym.sqrt( sym.power.Pow(x[0]-x[2],2)+sym.power.Pow(x[1]-x[3],2) )
    else:
        return math.sqrt( math.pow(x+w*np.sin(t)-xf,2)+math.pow(z+w*np.cos(t)-zf,2) )

def Dl(x, symbolic=False):
    raise NotImplementedError
    L = l(x,symbolic)
    # orient Dl(x) correctly
    return -1*np.array( [ (x[0]-x[2])/L, (x[1]-x[3])/L, (-x[0]+x[2])/L, (-x[1]+x[3])/L ] )

def v(kappa,x,symbolic=False):
    return kappa*(l(x,symbolic) - l0)

#vector fields
def f(t,x,p):
    G = np.array([0, -g, 0])
    return G

def f_springLeg(t,x,p, leg,symbolic=False):
    raise NotImplementedError
    #spring in leg
    kappa = 10
    index = np.arange(4*leg,4*leg+4)
    fI = np.dot( v(kappa,x[index],symbolic), Dl(x[index],symbolic) )
    if symbolic:
        f = np.zeros(8,dtype='object')
    else:
        f = np.zeros(8)
    f[index] = np.dot( v(kappa,x[index],symbolic), Dl(x[index],symbolic) )
    return f

def ic(theta, h = 1.0, l0 =l, w0 = w):
    '''
    theta - table tilt(radians)
    h - initial height of table top (of untilted table)
    l - length of table leg
    '''
    x = np.zeros(6)
    x[0] = 0
    x[1] = (2*h)*(1./1.6)
    x[2] = theta

    return x

def add_functions(*functions):
    # add functions that take three arguments
    def add(f,g):
        return lambda t,x,p: f(t,x,p) + g(t,x,p)
    return ft.reduce(add, functions)

def generate_table_model(rigid=False):
    table = self_manip.SelfManip(M)
    table._a_list = [af, ar]
    table._Da_list = [Daf, Dar]
    table._Dadt_list = [lambda t,q: np.zeros(3), lambda t,q: np.zeros(3)]
    # rigid table
    if rigid:
        table._Da_bilateral = []
    else:
        #springy table
        table._Da_bilateral = []#, DaA1, DaA2 ]

    # the vector field is the same
    f_dict = {}
    for key in it.product([True,False],repeat=2):
        f_dict[key] = f
    if not rigid:
        raise NotImplementedError
        #springy table
        f_dict[(True, False, False,True)] = add_functions(f ,ft.partial(f_springLeg, leg=0),
                                                ft.partial(f_springLeg, leg=1))
        f_dict[(True, False, False,False)] = add_functions(f , ft.partial(f_springLeg, leg=0))
        f_dict[(False,False, False,True)] = add_functions(f , ft.partial(f_springLeg, leg=1))

    table._f = f_dict
    table.num_q = 3
    return table

def run_sweep():
    table=generate_table_model(True)

    t = (0,1.5)
    t = (0,8.)
    p = table.P()
    p.j = np.array([False, False])
    h = 1e-3
    n = np.infty
    debug = False

    thetas = np.arange(-.3, .31, .01)
    thetas[np.abs(thetas)<1e-6] = 0. #force 0 rotation to be a value
    theta_dots = np.zeros_like(thetas)
    y_dots = np.zeros_like(thetas)
    x_dots = np.zeros_like(thetas)
    t = (0,4.43)
    for index, theta in enumerate(thetas):
        sys.stdout.write('.')
        sys.stdout.flush()
        x0 = ic(theta)
        trjs = rx.EulerSelfManipulation(t, x0, p, table, h, .001, 4, debug);
        trj = trjs[-1]
        qdot = trj.x[0][-3:]
        omega = qdot[2]
        theta_dots[index] = omega

    return thetas, theta_dots

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--animate", action="store_true")
    # sweep over the theta dot values
    parser.add_argument("--sweep", action="store_true")
    parser.add_argument("theta", type=float, nargs='?',   default=0.)
    args = parser.parse_args()

    rigid = True
    table=generate_table_model(True)

    t = (0,1.5)
    t = (0,8.)
    p = table.P()
    p.j = np.array([False, False])
    h = 0.01
    n = np.infty
    debug = False

    if args.sweep:
        thetas = np.arange(-.3, .31, .01)
        thetas[np.abs(thetas)<1e-6] = 0. #force 0 rotation to be a value
        theta_dots = np.zeros_like(thetas)
        y_dots = np.zeros_like(thetas)
        x_dots = np.zeros_like(thetas)
        t = (0,8.0)
        t = (0,4.43)
        for index, theta in enumerate(thetas):
            x0 = ic(theta)
            trjs = rx.EulerSelfManipulation(t, x0, p, table, h, .001, 4, debug);
            trj = trjs[-1]
            qdot = trj.x[0][-3:]
            omega = qdot[2]
            theta_dots[index] = omega
            y_dots[index] = qdot[1]
            x_dots[index] = qdot[0]
        plt.plot(thetas, theta_dots)
        if rigid:
            plt.title('Rigid')

        plt.legend(['Theta Dot'])
        plt.xlabel('Theta')
        plt.ylabel('Dot')

