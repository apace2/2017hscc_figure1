from __future__ import division
import relax as rx
import numpy as np
import math
import pylab as plt
import copy as cp

def dot(*args):
    return reduce(np.dot, args)

def inv(x):
    if len(np.shape(x))<2:
        return 1./x
    else:
        return np.linalg.inv(x)

class SelfManip(rx.HDS):
    def __init__(self,M):
        self.num_q = M.shape[0]
        self._Minv = np.linalg.inv(M)
        self._M = M
        self.gamma = lambda q: 0. #default to plastic impact
        # The user needs to set the below
        self._Dadt_list = []
        self._Da_list = []
        self._a_list = []
        self._f = {}
        self._Da_bilateral = []
        self._Dadt_bilateral = []
    
    def fI(self, t,x,p):
        # for additive force
        #return reduce( np.add, [func(t,x,c) for (func,c) in zip(f,p.j)])
        return self._f[tuple(p.j)](t,x,p)

    def a(self, t,x):
        return [a_tmp(t,x) for a_tmp in self._a_list]

    def Da(self, t,x):
        Da_tmp = np.vstack([Da_t(t,x) for Da_t in self._Da_list])
        return Da_tmp

    def Da_dt(self, t,x):
        Da_tmp = np.vstack([Da_dt(t,x) for Da_dt in self._Dadt_list])
        return Da_tmp

    def Da_bilateral(self,t,x,p):
        if len(self._Da_bilateral) == 0:
         #   if p.j.any():
         #       return np.zeros(len(x)/2)
         #   else:
            return np.array([]).reshape(0, len(x)//2)
        Da_tmp = np.vstack([Da_t(t,x) for Da_t in self._Da_bilateral])
        return Da_tmp

    def Da_dt_bilateral(self,t,x,p):
        if len(self._Da_bilateral) == 0:
         #   if p.j.any():
         #       return np.zeros(len(x)/2)
         #   else:
            return np.array([]).reshape(0, len(x)//2)
        Dadt_tmp = np.vstack([Da_dt(t,x) for Da_dt in self._Dadt_bilateral])
        return Dadt_tmp

    def Da_complete(self,t,x,p):
        Ab = self.Da_bilateral(t,x,p)
        A = self.Da(t,x)[np.ix_(p.j)]
        A = np.vstack( (A,Ab) )
        return A

    def Da_complete_index(self,t,x,p,index):
        Ab = self.Da_bilateral(t,x,p)
        A = self.Da(t,x)[np.ix_(index)]
        A = np.vstack( (A,Ab) )
        return A

    def dotA(self,t,x,p):
        Ab_dt = self.Da_dt_bilateral(t,x,p)
        A_dt = self.Da_dt(t,x)[np.ix_(p.j)]
        A_dt = np.vstack( (A_dt,Ab_dt) )
        if t>1. or p.j[0]:
            None;
        return A_dt

    def calc_lambda(self,t,x,p):
        fI_val = self.fI(t,x,p)

        Ab = self.Da_bilateral(t,x,p)
        A = self.Da(t,x)[np.ix_(p.j)]
        A = np.vstack( (A,Ab) )
        
        lamb = np.dot( inv(dot(A,self._Minv,A.T)), 
                dot(A,self._Minv,fI_val) + dot(self.dotA(t,x,p),x[self.num_q:]))
        #lamb = np.dot( inv(dot(A,self._Minv,A.T)), 
                #dot(A,self._Minv,fI_val))
        return lamb

    def _deactGuard(self,t,x,p, idx_on):
        # idx_on 
        dg =  np.dot(self.Da(t,x), x[self.num_q:])
    def G(self,t,x,p):
        # non active guards
        dg =  np.dot(self.Da(t,x), x[self.num_q:])
        #dg = (dg < 0.)
        dg = (dg < -0.01)
        guard = self.a(t,x)
        inactive_constraint = guard * dg

        #active guards
        lamb = self.calc_lambda(t,x,p)
        if len(self._Da_bilateral)!=0:
            # remove bilateral lambdas
            # unilateral constraints are first
            lamb = lamb[:-len(self._Da_bilateral)]
        active_constraint = np.zeros(len(p.j))
        active_constraint[p.j] = -lamb

        g = [inactive_constraint, active_constraint]

        g = [g[constraint_active][index] for (index,constraint_active) in enumerate(p.j)]
        return np.array(g)

    def E(self,t,z,p,v):
        return z+v

    def F(self,t,x,p):
        # Mqdotdot = fJ(q,qdot) + lambdaDaJ(q)
        fI_val = self.fI(t,x,p)

        Ab = self.Da_bilateral(t,x,p)
        A = self.Da(t,x)[np.ix_(p.j)]
        A = np.vstack( (A,Ab) )

        lamb = self.calc_lambda(t,x,p)
        vector_field = np.hstack( (x[self.num_q:], np.dot(self._Minv, fI_val - np.squeeze(np.dot(lamb,A) ))))
        #if np.any(p.j):
        #    import ipdb; ipdb.set_trace()
        return vector_field

    def _ResetMap(self, t,x,p,index):
        Ab = self.Da_bilateral(t,x,p)
        A = self.Da(t,x)
        A = A[np.ix_(index)]
        A = np.vstack( (A,Ab) )
        P = dot(self._Minv,A.T, inv(dot(A,self._Minv,A.T)),A)
        return  np.eye(P.shape[0]) - (1+self.gamma(x))*P

    def _ResetState(self,t,x,p,index):
        # index - list of unilateral constraints that are on
        if len(index)!=0: #list is not empty
            qdot_new = np.dot( self._ResetMap(t,x,p,index), x[self.num_q:])
        else:
            qdot_new = x[self.num_q:] # no reset

        qNew = np.hstack( (x[:self.num_q], qdot_new) )
        return qNew

    def R(self,t,x,p):
        a_val = self.a(t,x) 
        g = self.G(t,x,p)
        idx = g< 0 #the index of the guards causing the reset
        
        idx_on = np.logical_and(idx,~p.j) #the constraints becoming active
        #idx_off = np.bitwise_and(idx, p.j) #the constraints becoming inactive

        p = cp.deepcopy(p)
        p.j[np.ix_(idx)] = ~p.j[np.ix_(idx)]
        #import ipdb; ipdb.set_trace()
        xNew = self._ResetState(t,x,p,idx_on.nonzero()[0] )
        #check for simultaneous deact

        return t, xNew, p
